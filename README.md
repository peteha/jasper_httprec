# Jasper httprec
Cisco Jasper HTTP Receiver in Python

Takes Jasper REST Push output and sends it a Splunk HEC in JSON

Runs as a service with the use of the HTTPREC service.

run ./httprec.py

#### Requirements:

Python 3
- Flask
- xmltojson

