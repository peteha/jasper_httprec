## Created by Peter Hauck - 2019
#httprec.py
from flask import Flask, request, Response
import logging
import re
import rest_outputer as rop
import urllib.parse as up
import xmltodict

app = Flask(__name__)
log = logging.getLogger('werkzeug')
log.disabled = True

sitever = 2

event = "jasper"
spl = rop.splunk_cfg_validate()


def logger(logmsg):
    logging.basicConfig(filename=logout, level=logging.DEBUG, format='%(asctime)s %(message)s',
                        datefmt="%d/%m/%Y %H:%M:%S")
    logging.info(logmsg)


def prod_jasper_message(msg):
    a = {match.group(1): match.group(2)
         for match in re.finditer(r"(^[^=]+|(?<=&)\w+)=([^=]+)&", msg)
         }
    b = {match.group(1): match.group(2)
         for match in re.finditer(r"(data)=(.+$)", msg)
         }
    f_msg = dict(a, **b)
    f_msg['data'] = up.unquote(f_msg['data'])
    f_msg['data'] = f_msg['data'] .replace('<?xml+version="1.0"+encoding="UTF-8"+standalone="yes"?>', '')\
        .replace('+xmlns', '')\
        .replace('=','')\
        .replace('"http://api.jasperwireless.com/ws/schema"', '')
    f_msg['data'] = xmltodict.parse(f_msg['data'])
    rop.splunk_event(f_msg, spl)
    return f_msg


@app.route('/jasper', methods=['POST'])
def result():
    req_data = prod_jasper_message(request.get_data(as_text=True))
    response = Response(status=200)
    return response


@app.route('/ver')
def ver_form():
    return 'Site Version - ' + str(sitever)


if __name__ == '__main__':
    app.run(debug=False, port=2222, host='0.0.0.0')