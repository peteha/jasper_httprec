import json
import requests
import config
import rest_outputer as rop

spl = rop.splunk_cfg_validate()

def __isjson(myjson):
    try:
        json_object = json.loads(myjson)
    except ValueError:
        return False
    return True


def __getdata(get_url):
    headers = {'Content-Type': 'application/json', 'Authorization': config.api_key}
    geturl = config.base_url + get_url
    resp = requests.get(geturl, headers=headers)
    resp.connection.close()
    __isjson(resp.text)
    if resp.ok
    return resp.text

def _acct_devices(acct):
    get_url = '/devices?accountId=' + config.account_id + '&modifiedSince=2016-04-18T17%3A31%3A34%2B00'
    resp_text = __getdata(get_url)
    mod_json = json.loads(resp_text)
    for i in mod_json['devices']:
        i.update({'event': "acct_devices", 'account': acct})
        rop.splunk_event(i, spl)


def _acct_det(acct):
    get_url = '/customers/' + config.account_id
    resp_text = __getdata(get_url)
    mod_json = json.loads(resp_text)
    print(mod_json)
    #for i in mod_json['devices']:
    #    i.update({'event': "acct_devices", 'account': acct})
    #    rop.splunk_event(i, spl)

if __name__ == '__main__':
    _acct_det(config.account_id)

