#!/usr/bin/env python3
import config
import time
import json
import os
import sys
import requests
import socket

class __spl:
    host = ""
    token = ""
    auth = {}
    source = ""
    sourcetype = ""
    index = ""
    fields = ""
    url = ""
    https = True
    verify = False
    json = dict()
    hostname = ""
    host_ip = ""

splunk_url_suffix = "/services/collector/event"

def splunk_cfg_validate():
    # Splunk Configuration
    spl = __spl()

    spl.hostname = socket.gethostname()

    try:
        spl.host = config.SPLUNK_HOST
    except BaseException:
        print("SPLUNK_HOST not assigned in config.py")
        sys.exit()

    try:
        spl.port = config.SPLUNK_PORT
    except BaseException:
        print("SPLUNK_PORT not assigned in config.py")
        sys.exit()

    try:
        spl.token = config.SPLUNK_TOKEN
    except BaseException:
        print("SPLUNK_TOKEN not assigned in config.py")
        sys.exit()

    try:
        spl.index = config.SPLUNK_INDEX
    except BaseException:
        print("SPLUNK_INDEX not assigned in config.py")
        sys.exit()

    try:
        spl.source = config.SPLUNK_SOURCE
    except BaseException:
        print("SPLUNK_SOURCE not assigned in config.py")
        sys.exit()

    try:
        spl.sourcetype = config.SPLUNK_SRCTYPE
    except:
        print("SPLUNK_SRCTYPE not assigned in config.py")
        sys.exit()

    # Optional Variables
    try:
        spl.https = config.SPLUNK_HTTPS
    except BaseException:
        spl.https = True

    try:
        spl.verify = config.SPLUNK_HTTPSVERIFY
    except BaseException:
        spl.verify = False

    try:
        spl.time = config.SPLUNK_ASSIGNTIME
    except BaseException:
        spl.time = True

    # Build compound variables
    if spl.https is True:
        spl.url = "https://" + spl.host + ":" + spl.port + splunk_url_suffix
    else:
        spl.url = "http://" + spl.host + ":" + spl.port + splunk_url_suffix

    if spl.time is True:
        spl.json = ({"time": time.time(), "host": spl.hostname, "source": spl.source,
                    "sourcetype": spl.sourcetype, "index": spl.index})
    else:
        spl.json = ({"host": spl.hostname, "source": spl.source,
                    "sourcetype": spl.sourcetype, "index": spl.index})
    spl.auth = ({'Authorization': "Splunk " + spl.token})
    return spl

def splunk_event(event, spl):
    spl.json.update({"event": event})
    r = requests.post(spl.url, headers=spl.auth, json=spl.json, verify=spl.verify)
    r.connection.close()
    return r
